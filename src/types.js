export const BLUE_DATA = 'BLUE_DATA'
export const MAP_DATA = 'MAP_DATA'

export const SHOW_DATA = 'SHOW_DATA'
export const CHALLENGE_SELECTED = 'CHALLENGE SELECTED'

export const REC_SAVE = 'REC_SAVE'
export const RECORD = 'RECORD'
export const PAUSE = 'PAUSE'
export const STOP = 'STOP'