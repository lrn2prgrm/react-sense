import React from 'react';
import { MAP_DATA } from './types';
import { connect } from 'react-redux'

class MapConnector extends React.Component {

    componentDidMount() {
        navigator.geolocation.watchPosition(location => {
                const { latitude, longitude } = location.coords;
                this.props.dispatch({ type: MAP_DATA, value: {latitude, longitude}})
            },
            null,
            { enableHighAccuracy: true }
        );
    }

    render() { 
        return null;
    }
}
 
export default connect()(MapConnector);
