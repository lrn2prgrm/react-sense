import { createStore, combineReducers } from 'redux'
import * as types from './types';

const fastReducer = (state = {}, action) => {
    switch(action.type) {
        case types.MAP_DATA: 
            return {...state, mapData: action.value}
        case types.BLUE_DATA:
            return {...state, blueData: action.value}
        default:
            return state
    }
}

const recReducer = (state = {}, action) => {
    switch(action.type) {
        case types.REC_SAVE:
            const id = state.currentId || Date.now()
            return {// maybe i can just push this?
                ...state,
                [id]: [
                    ...(state[id] || []),
                    {
                        blueData: action.value.blueData,
                        mapData: {...action.value.mapData},
                        timestamp: Date.now()
                    }
                ],
                currentId: id
            }
        case types.RECORD:
            return {
                ...state,
                recording: true
            }            
        case types.PAUSE:
            return {
                ...state,
                recording: false,
            }                 
        case types.STOP:
            return {
                ...state,
                recording: false,
                currentId: null
            } 
        default:
            return state
    }
}

const otherReducer = (state = {}, action) => {
    switch(action.type) {
        case types.SHOW_DATA: 
            return {...state, showData: action.value}
        case types.CHALLENGE_SELECTED: 
            return {...state, isChallengeSelected: action.value}
        default:
            return state
    }
}

const masterReducer = combineReducers({
    fast: fastReducer,
    rec: recReducer,
    other: otherReducer
})

window.store = createStore(masterReducer);

export default window.store;