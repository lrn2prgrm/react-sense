import React from 'react';
import { connect } from 'react-redux'
import { REC_SAVE, STOP, PAUSE, RECORD, CHALLENGE_SELECTED } from './types'
import ExpVisual from './ExpVisual'

class ExpRecord extends React.Component {

    constructor(props) {
        super(props)
        this.playPause = this.playPause.bind(this)
    }

    componentDidUpdate() {
        if(!this.props.recording) {
            clearInterval(this.interval)
            this.interval = null
        }
    }

    playPause() {
        if(this.props.recording) {
            clearInterval(this.interval)
            this.interval = null
            this.props.dispatch({type: PAUSE})
        }
        else {
            this.props.dispatch({type: RECORD})
            this.interval = setInterval(
                () => this.props.dispatch({type: REC_SAVE, value: window.store.getState().fast}),
                500
            )
        }
    }

    stop = () => {
        this.props.dispatch({type: STOP})
        this.props.dispatch({type: CHALLENGE_SELECTED, value: false})
    }

    myData = () => {
        const currentId = this.props.currentId
        return currentId ? this.props.data[currentId] : []
    }
    
    render() {
        return <div>
                    <div className='group-btns-experiencia'>
                    <button className='btn-experiencia' onClick={this.playPause}>{this.props.recording ? 'pause' : 'play'}</button>
                    <button className='btn-experiencia' onClick={this.stop}>stop</button>
                    </div>
            {/*<p>data: {JSON.stringify(this.myData())}</p>*/}
            {<ExpVisual/>}
        </div>;
    }
}

const mapper = (state) => {
    return {
        data: state.rec,
        currentId: state.rec.currentId,
        recording: state.rec.recording,
    }
}

export default connect(mapper)(ExpRecord)
