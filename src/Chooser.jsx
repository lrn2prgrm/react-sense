import React from 'react';
import Experience from './Experience';
import { CHALLENGE_SELECTED } from './types'
import { connect } from 'react-redux' 
import './main.css'

class Chooser extends React.Component {

    componentDidMount() {
        window.showSlides(window.slideIndex);
        // Get the modal
        var modal = document.getElementById('myModal');
        this.modal = modal

        // Get the button that opens the modal
        var btn = document.getElementById("uv-experiencia-uno");
        
        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        
        // When the user clicks on the button, open the modal 
        btn.onclick = function() {
            modal.style.display = "block";
        }
        
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
        
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        }
    }

    prev(){
        window.plusSlides(-1)
    }

    next(){
        window.plusSlides(1)
    }

    selectChallenge = () => {
        this.modal.style.display = "none";
        this.props.dispatch({type: CHALLENGE_SELECTED, value: true})
    }

    hideStyle(def = 'block') {
        return { display: this.props.isChallengeSelected ? 'none': def }
    }

    render() {
        return (<React.Fragment><header style={this.hideStyle('flex')}>
            <div className="container-foto-ranking">
                <img src="imgs/perfil.jpg" alt="perfil" />
                <div id="ranking-perfil">
                    <p>4th <i className="fas fa-trophy"></i></p>
                    <p>2500 <i className="fas fa-star"></i></p>
                </div>
            </div>
            <div id="name-perfil">
                <p className="name">Leila Gutierrez</p>
                <p className="nivel">Investigador Jr  <i className="fas fa-flask"></i></p>
            </div>
        </header>
            <div className="slideshow-container" style={this.hideStyle()}>

                <div className="mySlides fade">
                    <img src="imgs/personajes/luminiscencia.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-luminiscencia/PRIMER-EXPERIENCIA-LUMINISCENCIA.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-luminiscencia/SEGUNDA-EXPERIENCIA-LUMINISCENCIA.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-luminiscencia/TERCERA-EXPERIENCIA-LUMINISCENCIA.png" style={{width: '100%'}} alt='an img' />
                </div>

                <div className="mySlides fade">
                    <img src="imgs/personajes/TEMPERATURA.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-temperatura/primera-experiencia-temperatura.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-temperatura/segunda-experiencia-temperatura.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-temperatura/tercera-experiencia-temperatura.png" style={{width: '100%'}} alt='an img' />
                </div>

                <div className="mySlides fade">
                    <img src="imgs/personajes/humedad.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-humedad/primera-experiencia-humedad.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-humedad/segunda-experiencia-humedad.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-humedad/tercera-experiencia-humedad.png" style={{width: '100%'}} alt='an img' />
                </div>

                <div className="mySlides fade">
                    <img src="imgs/personajes/uv.png" style={{width: '100%'}} alt='an img' />
                    <img id="uv-experiencia-uno" src="imgs/experiencias-uv/primera-experiencia-uv.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-uv/segunda-experiencia-uv.png" style={{width: '100%'}} alt='an img' />
                    <img src="imgs/experiencias-uv/tercera-experiencia-uv.png" style={{width: '100%'}} alt='an img' />
                </div>
                <a className="prev" onClick={this.prev}>&#10094;</a>
                <a className="next" onClick={this.next}>&#10095;</a>
            </div>
            <div id="myModal" className="modal" style={this.hideStyle('none')}>
                <div className="modal-content">
                    <span className="close">&times;</span>
                    <p className="frase">La radiación ultravioleta en el Perú alcanza en febrero sus registros más altos. Mientras que en Europa llega a un índice máximo de 10, en la sierra central y sur peruana se registra el doble.</p>
                    <br />
                    <strong>RECOMENDACIONES:</strong>
                    <ul>
                        <li>LLeva un gorro que te protega de los rayos ultravioleta.</li>
                        <li>Lleva un bloqueador solar a tu aventura.</li>
                        <li>usa un polo manga larga para proteger tu piel de los rayos ultravioleta.</li>
                    </ul>
                    <p className="btn" onClick={this.selectChallenge}>Hagamos Ciencia</p>
                </div>
            </div>
            {this.props.isChallengeSelected && <Experience/>}
            </React.Fragment>
        );
    }
}

const mapper = state => {
    return {
        isChallengeSelected: state.other.isChallengeSelected
    }
}
export default connect(mapper)(Chooser);
