import React, { Component } from 'react';
import BlueConnector from './BlueConnector';
import MapConnector from './MapConnector';

class App extends Component {
  render() {
    return (
      <div>
          <MapConnector/>
          <BlueConnector/>
      </div>
    );
  }
}

export default App;
