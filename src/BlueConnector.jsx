import React from 'react';
import { connect } from 'react-redux';
import { BLUE_DATA } from './types';
import Chooser from './Chooser';
import './home.css'

class BlueConnector extends React.Component {
    state = { 
        loading: false,
        success: false,
    }

    constructor(props) {
        super(props)
        this.connect = this.connect.bind(this);
        this.handleData = this.handleData.bind(this);
    }

    handleData(e) {
        const buf = e.target.value.buffer
        const text = String.fromCharCode.apply(null, new Uint8Array(buf))
        this.props.dispatch({ type: BLUE_DATA, value: text})
    }

    connect() {
        navigator.bluetooth.requestDevice({
            filters: [{
              services: [
                '0000ffe0-0000-1000-8000-00805f9b34fb',
              ]
            }]
        })
        .then(function (device) {
            console.log(device)
            return device.gatt.connect();
        })
        .then(server => {
            console.log('XD')
            this.setState({loading: false, success: true})
            return server.getPrimaryService('0000ffe0-0000-1000-8000-00805f9b34fb');
        })
        .then(function (service) {
            return service.getCharacteristic('0000ffe1-0000-1000-8000-00805f9b34fb');
        })
        .then(characteristic => characteristic.startNotifications())
        .then(characteristic =>
            characteristic.addEventListener('characteristicvaluechanged', this.handleData)
        )
        .catch(function (error) {
            console.log(error)
        });
    }

    render() { 
        return this.state.success ? 
            <Chooser/> :
            <div className='home'>
                <img className='logo' src="imgs/yatiqiri.png" alt="logo"/>
                <div className='connect' style={{textAlign: 'center'}}>
                    {this.state.loading ? 'Connecting' : <button onClick={this.connect}>Conéctate</button>}
                </div>
                
            </div>
            
    }
}

export default connect()(BlueConnector);
